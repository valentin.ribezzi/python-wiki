import sys
import datetime
import threading as th

import requests
from bs4 import BeautifulSoup
from colorama import Style, Fore
from pyfiglet import Figlet

# configuration
gameTitle = 'WikiGame'
url = 'https://fr.wikipedia.org/wiki/Sp%C3%A9cial:Page_au_hasard'
linksPerPage = 20
listItemsInput = []
listPageInput = ['prev', 'precedent', 'next', 'suivant']
windowWidth = 1920
windowHeight = 1080
backgroundColor = 'white'

# variables
modes = {'-h': 'Historique', '-t': 'Timer'}
history = []
links = []
ordoredList = []
startPage = ''
endPage = ''
currentPage = None
page = 1
turn = 1
countdown = None
timer = None


# function to get all content on a page
def getPageContent(path=None):
    return BeautifulSoup(
        requests.get('https://fr.wikipedia.org/wiki/' + (
            path if path is not None else 'Sp%C3%A9cial:Page_au_hasard')).content,
        'html.parser')


# function to get all page links (only those which starting with /wiki/ and which not contains :)
def createLinkList(html):
    for response in html.find(id="bodyContent").find_all('a'):
        if response.get('href', '/').startswith('/wiki/') and response.get('href', '/').find(':') == -1:
            links.append([response.text, response.get('href', '/').split('/')[2]])


# function to get the title of the page
def getPageTitle(pageContent):
    return pageContent.find('h1').text


# function to split each pages in multiples array in a variable
def setOrdoredList():
    pages = round(len(links) / 20)
    for pageList in range(pages):
        ordoredList.append(links[slice(linksPerPage * ((pageList + 1) - 1), (pageList + 1) * linksPerPage)])


# function which stopping the program
def stopProgram():
    print(Fore.RED + 'Vous avez dépassé le temps ecoulé de : ' + str(countdown) + ' secondes' + Fore.RESET)
    sys.exit()


# function which managing the game
def game(startPage, endPage, currentPage, page=1, turn=1):
    global countdown
    global timer

    # printing modes
    print(Fore.YELLOW + '------ Modes activés ------' + Fore.RESET)
    if sys.argv is not None:
        for index, arg in enumerate(sys.argv):
            if arg.startswith('-'):
                if index + 1 == len(sys.argv):
                    print('[ ' + modes[arg] + ' ]\n')
                else:
                    print('[ ' + modes[arg] + ' ]')
    else:
        print('Aucun mode n\' activé')

    # retrieval of the number of seconds in the timer
    if sys.argv[2] == '-t' and page == 1 and turn == 1:
        while True:
            try:
                countdown = int(input('Veuillez saisir une durée en secondes pour réaliser le wikigame : \n'))
            except ValueError:
                print(Fore.RED + 'Vous devez choisir un nombre\n' + Fore.RESET)
                continue
            else:
                if countdown > 0:
                    print(
                        Fore.LIGHTCYAN_EX +
                        'Vous avez choisi un timer de : ' + str(countdown) + ' secondes soit : ' + str(
                            datetime.timedelta(seconds=countdown)) + ' (H:M:S) \n'
                        + Fore.RESET
                    )
                    timer = th.Timer(countdown, stopProgram)
                    timer.start()
                    break
                else:
                    print(Fore.RED + 'Vous devez choisir un nombre supérieur à 0\n' + Fore.RESET)
                    continue

    # Printing page number
    print(Fore.YELLOW + '------ Tour ' + str(turn) + ' ------ \n' + Fore.RESET)

    # Printing page number
    print(Fore.YELLOW + '------ Page ' + str(page) + ' ------ \n' + Fore.RESET)

    # printing page name
    print('Page de début : ' + Fore.LIGHTCYAN_EX + getPageTitle(startPage) + Fore.RESET)
    print('Page de fin : ' + Fore.LIGHTCYAN_EX + getPageTitle(endPage) + Fore.RESET + '\n')

    if currentPage is not None:
        print('Page actuelle : ' + str(currentPage[0]) + '\n')

    # clear links / ordered list
    ordoredList.clear()
    links.clear()

    # generate all links
    if currentPage is not None:
        history.append(currentPage) if ('-h' in sys.argv) else ''
        createLinkList(getPageContent(currentPage[1]))
    else:
        history.append(startPage) if ('-h' in sys.argv) else ''
        createLinkList(startPage)

    # splitting list per pages
    setOrdoredList()

    # setting input
    listItemsInput = [str(index + 1) for index, link in enumerate(ordoredList[page - 1])]

    # displaying all links
    for index, link in enumerate(ordoredList[page - 1]):
        print(Fore.BLUE + '[' + str(index + 1) + ']' + ' : ' + str(link[0]) + Fore.RESET)

    # user choice
    choice = str(
        input(Fore.GREEN + '\nVeuillez choisir un lien dans cette page ou Changer de page\n' + Fore.RESET)).lower()

    while currentPage != endPage or countdown:
        try:
            # testing if choice is a good input to use
            assert choice in listItemsInput + listPageInput

            # testing if choice is a key of an item
            if choice in listItemsInput:
                currentPage = ordoredList[page - 1][int(choice) - 1]
                game(startPage, endPage, currentPage, page, turn + 1)

            # testing if choice is 'prev' or 'precedent'
            elif choice in ['prev', 'precedent']:
                if page == 1:
                    print('Vous ne pouvez pas aller à la page précédente')
                    return
                else:
                    game(startPage, endPage, currentPage, page - 1, turn)

            # testing if choice is 'next' or 'suivant'
            elif choice in ['next', 'suivant']:
                if page == len(ordoredList):
                    print('Vous ne pouvez pas aller à la page suivante')
                    return
                else:
                    game(startPage, endPage, currentPage, page + 1, turn)

        # if choice isn't a good input to use
        except AssertionError:
            print('Veuillez saisir une valeur correcte.')
            sys.exit()

    print('BRAVO, vous avez terminé le jeu en ' + str(turn) + 'tours')

    # display the pages history
    print(Fore.MAGENTA + '------ Historique des pages ------ \n' + Fore.RESET) if ('-h' in sys.argv) else ''
    for index, item in enumerate(history):
        print('[' + str(index + 1) + ']' + ' : ' + item) if ('-h' in sys.argv) else ''


# setting page name
startPage = getPageContent()
endPage = getPageContent()

# Creating stylized title with custom font
print(Style.BRIGHT + Figlet(font='ogre').renderText(gameTitle) + Style.RESET_ALL)

game(startPage, endPage, currentPage, page=1, turn=1)
