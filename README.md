# Projet WikiGame - Valentin RIBEZZI

## Informations
Elève : Valentin RIBEZZI  
Spécialité : Ingénierie du Web

## Packages à installer

```bash
pip install requests
pip install beautifulsoup4
pip install colorama
pip install pyfilget
```

## NB
Ce projet n'est que sous forme "Terminal" car je ne suis pas parvenu à le mettre en forme....
J'ai néanmoins ajouté des fonctionnalités demandées dans la V3 pour faire un jeu plus complet que simplement la V1.